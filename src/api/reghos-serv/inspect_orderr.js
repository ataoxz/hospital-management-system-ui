import request from '@/utils/request'

// 查询检查项目订单列表
export function listInspect_orderr(query) {
  return request({
    url: '/reghos-serv/inspect_orderr/list',
    method: 'get',
    params: query
  })
}

// 查询检查项目订单详细
export function getInspect_orderr(inspectOrderId) {
  return request({
    url: '/reghos-serv/inspect_orderr/' + inspectOrderId,
    method: 'get'
  })
}

// 新增检查项目订单
export function addInspect_orderr(data) {
  return request({
    url: '/reghos-serv/inspect_orderr',
    method: 'post',
    data: data
  })
}

// 修改检查项目订单
export function updateInspect_orderr(data) {
  return request({
    url: '/reghos-serv/inspect_orderr',
    method: 'put',
    data: data
  })
}

// 删除检查项目订单
export function delInspect_orderr(inspectOrderId) {
  return request({
    url: '/reghos-serv/inspect_orderr/' + inspectOrderId,
    method: 'delete'
  })
}
