import request from '@/utils/request'

// 查询血常规结果列表
export function listRoutine(query) {
  return request({
    url: '/reghos-serv/routine/list',
    method: 'get',
    params: query
  })
}

// 查询血常规结果详细
export function getRoutine(brId) {
  return request({
    url: '/reghos-serv/routine/' + brId,
    method: 'get'
  })
}

// 新增血常规结果
export function addRoutine(data) {
  return request({
    url: '/reghos-serv/routine',
    method: 'post',
    data: data
  })
}

// 修改血常规结果
export function updateRoutine(data) {
  return request({
    url: '/reghos-serv/routine',
    method: 'put',
    data: data
  })
}

// 删除血常规结果
export function delRoutine(brId) {
  return request({
    url: '/reghos-serv/routine/' + brId,
    method: 'delete'
  })
}
