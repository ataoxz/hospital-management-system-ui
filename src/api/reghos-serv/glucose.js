import request from '@/utils/request'

// 查询血糖结果列表
export function listGlucose(query) {
  return request({
    url: '/reghos-serv/glucose/list',
    method: 'get',
    params: query
  })
}

// 查询血糖结果详细
export function getGlucose(bgId) {
  return request({
    url: '/reghos-serv/glucose/' + bgId,
    method: 'get'
  })
}

// 新增血糖结果
export function addGlucose(data) {
  return request({
    url: '/reghos-serv/glucose',
    method: 'post',
    data: data
  })
}

// 修改血糖结果
export function updateGlucose(data) {
  return request({
    url: '/reghos-serv/glucose',
    method: 'put',
    data: data
  })
}

// 删除血糖结果
export function delGlucose(bgId) {
  return request({
    url: '/reghos-serv/glucose/' + bgId,
    method: 'delete'
  })
}
