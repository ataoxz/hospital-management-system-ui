import request from '@/utils/request'


export function getUserList(depId) {
  return request({
    url: '/system/user/userListByDepId',
    method: 'get',
    params: {
      depId:depId
    }
  })
}


export function getHaha(user) {
  return request({
    url: '/reghos-serv/schedule/haha',
    method: 'get',
    params: user
  })
}

export function getHaha2(user) {
  return request({
    url: '/system/schedule/haha2',
    method: 'post',
    data: user
  })
}

export function getHaha3() {
  return request({
    url: '/system/schedule/haha3/' + 1,
    method: 'delete'
  })
}

export function getHaha4() {
  return request({
    url: '/system/schedule/haha4/' + 2,
    method: 'delete'
  })
}

// 查询排班列表
export function listSchedule(query) {
  return request({
    url: '/system/schedule/list',
    method: 'get',
    params: query
  })
}

// 查询排班详细
export function getSchedule(scheduleId) {
  return request({
    url: '/system/schedule/' + scheduleId,
    method: 'get'
  })
}

// 新增排班
export function addSchedule(data) {
  return request({
    url: '/system/schedule',
    method: 'post',
    data: data
  })
}

// 修改排班
export function updateSchedule(data) {
  return request({
    url: '/system/schedule',
    method: 'put',
    data: data
  })
}

// 删除排班
export function delSchedule(scheduleId) {
  return request({
    url: '/system/schedule/' + scheduleId,
    method: 'delete'
  })
}
