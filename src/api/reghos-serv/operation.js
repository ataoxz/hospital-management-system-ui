import request from '@/utils/request'

// 查询手术项目列表
export function listOperation(query) {
  return request({
    url: '/reghos-serv/operation/list',
    method: 'get',
    params: query
  })
}

// 查询手术项目详细
export function getOperation(operationId) {
  return request({
    url: '/reghos-serv/operation/' + operationId,
    method: 'get'
  })
}

// 新增手术项目
export function addOperation(data) {
  return request({
    url: '/reghos-serv/operation',
    method: 'post',
    data: data
  })
}

// 修改手术项目
export function updateOperation(data) {
  return request({
    url: '/reghos-serv/operation',
    method: 'put',
    data: data
  })
}

// 删除手术项目
export function delOperation(operationId) {
  return request({
    url: '/reghos-serv/operation/' + operationId,
    method: 'delete'
  })
}
