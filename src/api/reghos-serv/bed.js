import request from '@/utils/request'

// 查询病床床位列表
export function listBed(query) {
  return request({
    url: '/reghos-serv/bed/list',
    method: 'get',
    params: query
  })
}

// 查询病床床位详细
export function getBed(bedId) {
  return request({
    url: '/reghos-serv/bed/' + bedId,
    method: 'get'
  })
}

// 新增病床床位
export function addBed(data) {
  return request({
    url: '/reghos-serv/bed',
    method: 'post',
    data: data
  })
}

// 修改病床床位
export function updateBed(data) {
  return request({
    url: '/reghos-serv/bed',
    method: 'put',
    data: data
  })
}

// 删除病床床位
export function delBed(bedId) {
  return request({
    url: '/reghos-serv/bed/' + bedId,
    method: 'delete'
  })
}
