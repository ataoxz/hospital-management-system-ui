import request from '@/utils/request'


// 查询挂号根据身份证
export function doGuoHao(registerId) {
  return request({
    url: '/reghos-serv/register/doGuoHao',
    method: 'get',
    params: {
      registerId: registerId
    }
  })
}

// 查询挂号根据身份证
export function endReg(registerId) {
  return request({
    url: '/reghos-serv/register/endReg',
    method: 'get',
    params: {
      registerId: registerId
    }
  })
}

// 查询挂号根据身份证
export function getRegisterByCardId(cardId) {
  return request({
    url: '/reghos-serv/register/getRegisterByCardId',
    method: 'get',
    params: {
      cardId: cardId
    }
  })
}

// 查询挂号列表根据当前医生
export function queryRegisterListByUser(carId) {
  return request({
    url: '/reghos-serv/register/queryRegisterListByUser',
    method: 'get',
  })
}

// 查询挂号列表根据当前医生
export function getRegisterById(registerId) {
  return request({
    url: '/reghos-serv/register/getRegisterById',
    method: 'get',
    params: {
      registerId:registerId
    }
  })
}

// 查询挂号列表
export function listRegister(query) {
  return request({
    url: '/reghos-serv/register/list',
    method: 'get',
    params: query
  })
}

// 查询挂号详细
export function getRegister(registerId) {
  return request({
    url: '/reghos-serv/register/' + registerId,
    method: 'get'
  })
}

// 新增挂号
export function addRegister(data) {
  return request({
    url: '/reghos-serv/register',
    method: 'post',
    data: data
  })
}

// 修改挂号
export function updateRegister(data) {
  return request({
    url: '/reghos-serv/register',
    method: 'put',
    data: data
  })
}

// 删除挂号
export function delRegister(registerId) {
  return request({
    url: '/reghos-serv/register/' + registerId,
    method: 'delete'
  })
}
