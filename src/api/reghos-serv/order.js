import request from '@/utils/request'

// 查询药品订单列表
export function listOrder(query) {
  return request({
    url: '/reghos-serv/medicine_order/list',
    method: 'get',
    params: query
  })
}

// 查询药品订单详细
export function getOrder(moId) {
  return request({
    url: '/reghos-serv/medicine_order/details/' + moId,
    method: 'get'
  })
}

// 查询处方详情
export function queryPrescription(prescriptionId) {
  return request({
    url: '/reghos-serv/medicine_order/queryPrescription/' + prescriptionId,
    method: 'get'
  })
}

// 查询药品订单详细
export function getOrder2(moId) {
  return request({
    url: '/reghos-serv/medicine_order/' + moId,
    method: 'get'
  })
}

// 新增药品订单
export function addOrder(data) {
  return request({
    url: '/reghos-serv/medicine_order',
    method: 'post',
    data: data
  })
}

// 修改药品订单
export function updateOrder(data) {
  return request({
    url: '/reghos-serv/medicine_order',
    method: 'put',
    data: data
  })
}

// 删除药品订单
export function delOrder(moId) {
  return request({
    url: '/reghos-serv/medicine_order/' + moId,
    method: 'delete'
  })
}

// 条件查询处方
export function getPres(registerId,patientName) {
  return request({
    url: '/reghos-serv/medicine_order/getPres?registerId='+registerId+"&patientName="+patientName,
    method: 'get'
  })
}

// 模糊查询药品
export function getMedicines(name) {
  return request({
    url: '/reghos-serv/medicine_order/medicines/' + name,
    method: 'get'
  })
}

// 买药
export function buyMed(list,id) {
  return request({
    url: '/reghos-serv/medicine_order/buyMed?id='+id,
    method: 'post',
    data:{
      list:list
    }
    
  })
}