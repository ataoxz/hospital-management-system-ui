import request from '@/utils/request'

// 查询检查项目列表
export function listInspect(query) {
  return request({
    url: '/reghos-serv/inspect/list',
    method: 'get',
    params: query
  })
}

// 查询检查项目详细
export function getInspect(inspectId) {
  return request({
    url: '/reghos-serv/inspect/' + inspectId,
    method: 'get'
  })
}

// 新增检查项目
export function addInspect(data) {
  return request({
    url: '/reghos-serv/inspect',
    method: 'post',
    data: data
  })
}

// 修改检查项目
export function updateInspect(data) {
  return request({
    url: '/reghos-serv/inspect',
    method: 'put',
    data: data
  })
}

// 删除检查项目
export function delInspect(inspectId) {
  return request({
    url: '/reghos-serv/inspect/' + inspectId,
    method: 'delete'
  })
}
