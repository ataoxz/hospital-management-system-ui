import request from '@/utils/request'

// 查询手术订单列表
export function listOperation_order(query) {
  return request({
    url: '/reghos-serv/operation_order/list',
    method: 'get',
    params: query
  })
}

// 查询手术订单详细
export function getOperation_order(ooId) {
  return request({
    url: '/reghos-serv/operation_order/' + ooId,
    method: 'get'
  })
}

// 新增手术订单
export function addOperation_order(data) {
  return request({
    url: '/reghos-serv/operation_order',
    method: 'post',
    data: data
  })
}

// 修改手术订单
export function updateOperation_order(data) {
  return request({
    url: '/reghos-serv/operation_order',
    method: 'put',
    data: data
  })
}

// 删除手术订单
export function delOperation_order(ooId) {
  return request({
    url: '/reghos-serv/operation_order/' + ooId,
    method: 'get',
  })
}

// 查询用户姓名
export function getUserNameList() {
  return request({
    url: '/reghos-serv/operation_order/UserNamelist',
    method: 'get',
  })
}
