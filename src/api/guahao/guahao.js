
import request from '@/utils/request'
// 挂号方法
export function goGuahao(scheduleId, patientCardId) {
  return request({
    url: '/reghos-serv/guahao/guahao',
    method: 'get',
    params: {
        scheduleId: scheduleId,
        patientCardId: patientCardId
    }
  })
}

export function addPatient(patient) {
    return request({
      url: '/customer-serv/patient',
      method: 'post',
      data: patient
    })
  }