import request from "@/utils/request";

export function recharge(money) {
  return request({
    url: "/customer-serv/customer/recharge/" + money,
    method: "post",
  });
}
