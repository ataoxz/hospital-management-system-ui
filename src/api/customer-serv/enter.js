import request from '@/utils/request'

// 出院方法
export function goChuYuan(enter) {
  return request({
    url: '/customer-serv/enter/goChuYuan',
    method: 'post',
    data: enter
  })
}

// 住院方法
export function goZhuYuan(registerId) {
    return request({
      url: '/customer-serv/enter/goZhuYuan',
      method: 'get',
      params: {
        registerId:registerId
      }
    })
}

// 新增病历
export function addRecord(record) {
  return request({
    url: '/customer-serv/record',
    method: 'post',
    data: record
  })
}

// 查询住院单根据身份证号
export function getEnterByCardId(cardId) {
  return request({
    url: '/customer-serv/enter/getEnterByCardId',
    method: 'get',
    params: {
      cardId:cardId
    }
  })
}

// 查询住院单根据身份证号
export function getBedListByDepId(depId) {
  return request({
    url: '/reghos-serv/bed/getBedListByDepId',
    method: 'get',
    params: {
      depId:depId
    }
  })
}

// 分配床位
export function goFBed(enter) {
  return request({
    url: '/customer-serv/enter/goFBed',
    method: 'post',
    data: enter
  })
}