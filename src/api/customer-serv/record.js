import request from '@/utils/request'

// 查询病历列表
export function listRecord(query) {
  return request({
    url: '/customer-serv/record/list',
    method: 'get',
    params: query
  })
}

// 查询病历详细
export function getRecord(recordId) {
  return request({
    url: '/customer-serv/record/' + recordId,
    method: 'get'
  })
}

// 新增病历
export function addRecord(data) {
  return request({
    url: '/customer-serv/record',
    method: 'post',
    data: data
  })
}

// 修改病历
export function updateRecord(data) {
  return request({
    url: '/customer-serv/record',
    method: 'put',
    data: data
  })
}

// 删除病历
export function delRecord(recordId) {
  return request({
    url: '/customer-  serv/record/' + recordId,
    method: 'delete'
  })
}
